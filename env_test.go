package env_test

import (
	"errors"
	"io"
	"io/fs"
	"os"
	"strconv"
	"strings"
	"testing"
	"time"

	"code.soquee.net/env"
)

var loadEnvTestCases = [...]struct {
	env      string
	expected map[string]string
	err      bool
}{
	0: {},
	1: {
		env: `#`,
	},
	2: {
		env: `#test`,
	},
	3: {
		env: `  # test=test`,
	},
	4: {
		env: `LOAD_ENV_TEST=hi`,
		expected: map[string]string{
			"LOAD_ENV_TEST": "hi",
		},
	},
	5: {
		env: `	SPACE_TEST	= 	hi `,
		expected: map[string]string{
			"LOAD_ENV_TEST": "",
			"SPACE_TEST":    "hi",
		},
	},
	6: {
		env: `ANOTHER_TEST=`,
		expected: map[string]string{
			"ANOTHER_TEST": "",
		},
	},
	7: {
		env: `=check`,
		err: true,
	},
	8: {
		env: `BAD`,
		err: true,
	},
	9: {
		env: `TEST_VAR_A=1

TEST_VAR_B=2`,
		expected: map[string]string{
			"TEST_VAR_A": "1",
			"TEST_VAR_B": "2",
		},
	},
	10: {
		env: "TEST_VAR_A\t= \"unquoteme\t\"\nTEST_VAR_B='u'\nTEST_VAR_C  =` unbacktickme`\nTEST_VAR_D='nounquote'",
		expected: map[string]string{
			"TEST_VAR_A": "unquoteme\t",
			"TEST_VAR_B": "u",
			"TEST_VAR_C": " unbacktickme",
			"TEST_VAR_D": "'nounquote'",
		},
	},
	11: {
		env: `FOO=BAR=BAZ
		BAR=BAZ #NOT A COMMENT`,
		expected: map[string]string{
			"FOO": "BAR=BAZ",
			"BAR": "BAZ #NOT A COMMENT",
		},
	},
}

func TestLoadEnv(t *testing.T) {
	for i, tc := range loadEnvTestCases {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			err := env.Read(strings.NewReader(tc.env))
			switch {
			case err != nil && !tc.err:
				t.Fatalf("Unexpected error: %q", err)
			case err == nil && tc.err:
				t.Fatal("Expected test to error")
			}

			for key, value := range tc.expected {
				if val := os.Getenv(key); val != value {
					t.Errorf("Unexpected value for env variable %q: want=%q, got=%q", key, value, val)
				}
				_ = os.Unsetenv(key)
			}
		})
	}
}

const envFile = `foo=bar`

type vfsFile struct {
	fd      io.Reader
	envFile string
	closed  bool
}

func (vfsFile) Name() string {
	return ".env"
}

func (vfsFile) Size() int64 {
	return int64(len(envFile))
}

func (vfsFile) Mode() fs.FileMode {
	return fs.ModePerm
}

func (vfsFile) ModTime() time.Time {
	return time.Now()
}

func (vfsFile) IsDir() bool { return false }

func (vfsFile) Sys() any { return nil }

func (v *vfsFile) Stat() (fs.FileInfo, error) {
	return v, nil
}

func (v *vfsFile) Read(p []byte) (int, error) {
	return v.fd.Read(p)
}

func (v *vfsFile) Close() error {
	v.closed = true
	return nil
}

type vfs struct{}

func (vfs) Open(name string) (fs.File, error) {
	if name != ".env" {
		return nil, fs.ErrNotExist
	}
	return &vfsFile{
		fd: strings.NewReader(envFile),
	}, nil
}

func TestReadFS(t *testing.T) {
	files := &vfs{}
	err := env.ReadFS(files, "foo")
	if !errors.Is(err, fs.ErrNotExist) {
		t.Fatalf("expected file not to exist, but got error %v", err)
	}

	err = env.ReadFS(files, ".env")
	if err != nil {
		t.Fatalf("unexpected error opening .env file: %v", err)
	}
	if v := os.Getenv("foo"); v != "bar" {
		t.Fatalf("unexpected error reading file from FS: want=%v, got=%v", "bar", v)
	}
}

func TestReadFile(t *testing.T) {
	err := env.ReadFile("testdata/foo")
	if !errors.Is(err, fs.ErrNotExist) {
		t.Fatalf("expected file not to exist, but got error %v", err)
	}

	err = env.ReadFile("testdata/env")
	if err != nil {
		t.Fatalf("unexpected error opening env file: %v", err)
	}
	if v := os.Getenv("bar"); v != "baz" {
		t.Fatalf("unexpected error reading file from FS: want=%v, got=%v", "baz", v)
	}
}
