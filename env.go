// Package env can be used to load environment variables from files.
//
// The following is a complete example of a file that can be parsed by this
// package:
//
//	# Lines beginning with "#" are comments and are skipped.
//
//	# Comments are not supported at the end of lines, this
//	# will result in a variable "Foo" with the value "Bar #Baz"
//	Foo=Bar #Baz
//
//	# Whitespace is trimmed with strings.TrimSpace. To include
//	# spaces, wrap the value in "double quotes", or `back ticks`.
//	# The following results in "Foo" having the value "Bar ".
//	Foo = "Bar "
//
//	# Finally, single characters may be quoted with single quotes.
//	# If single quotes are used on something that is not a single
//	# character they are not unwrapped and are part of the string.
//	# The following results in "Foo" having the value "'Bar'" and
//	# "Bar" having the value "b".
//	Foo='Bar'
//	Bar='b'
package env // import "code.soquee.net/env"

import (
	"bufio"
	"fmt"
	"io"
	"io/fs"
	"os"
	"strconv"
	"strings"
)

// ReadFile is like Read except that it opens and reads from a file.
func ReadFile(fpath string) error {
	fd, err := os.Open(fpath)
	if err != nil {
		return err
	}
	/* #nosec */
	defer fd.Close()

	return Read(fd)
}

// ReadFS is like Read except that it opens and reads from a file in a
// filesystem.
func ReadFS(vfs fs.FS, fpath string) error {
	fd, err := vfs.Open(fpath)
	if err != nil {
		return err
	}
	/* #nosec */
	defer fd.Close()

	return Read(fd)
}

// Read loads a list of environment variables from a reader and puts them into
// the processes local environment.
func Read(fd io.Reader) error {
	scanner := bufio.NewScanner(fd)
	line := 0
	for scanner.Scan() {
		text := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(text, "#") || text == "" {
			continue
		}
		parts := strings.SplitN(text, "=", 2)
		if len(parts) != 2 {
			return fmt.Errorf("invalid .env line %d", line)
		}
		val := strings.TrimSpace(parts[1])
		unquoted, err := strconv.Unquote(val)
		// ignore any errors, assume that means the original string wasn't quoted.
		if err == nil {
			val = unquoted
		}
		err = os.Setenv(strings.TrimSpace(parts[0]), val)
		if err != nil {
			return err
		}
		line++
	}
	return scanner.Err()
}
